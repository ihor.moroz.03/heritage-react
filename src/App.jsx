import React, { createContext, useEffect, useMemo, useState } from "react";
import { BrowserRouter as Router, Route, Link, Routes, Outlet } from "react-router-dom";
import { Navbar, Nav, NavDropdown, Button, ToastContainer, Toast } from "react-bootstrap";
import Monuments from "./Monuments";
import NotFound from "./NotFound";
import Orders from "./Orders";
import Login from "./Login";
import { client } from "./client";
import AdminCommissionProtocols from "./AdminCommissionProtocols";
import Users from "./Users";
import "./styles.css";

export const ToastContext = createContext(null);
export const UserContext = createContext(null);

function App() {
  const [role, setRole] = useState("");
  const [toasts, setToasts] = useState([]);
  const userContextValue = useMemo(() => ({ role }), [role]);

  //add useCallback hook
  const addToast = (title, message, variant) => {
    const id = new Date().getTime();
    setToasts([...toasts, { id, title, message, variant }]);
  };

  useEffect(() => {
    const role = localStorage.getItem("role");
    if (role) setRole(role);
  });

  return (
    <UserContext.Provider value={userContextValue}>
      <ToastContext.Provider value={addToast}>
        <Router>
          <Navbar bg="light" expand="lg">
            <Navbar.Brand>
              <h1 style={{ color: "green" }}>Heritage</h1>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ml-auto">
                <Nav.Link as={Link} to="/" exact="true">
                  Пам'ятки
                </Nav.Link>
                {role === "User" || role === "Admin" ? (
                  <>
                    {/* <Nav.Link as={Link} to="/orders">
                      Приписи
                    </Nav.Link>
                    <Nav.Link as={Link} to="/admincommissionprotocols">
                      Адміністративні правопорушення
                    </Nav.Link> */}
                  </>
                ) : (
                  <></>
                )}
                {role === "Admin" ? (
                  <>
                    <Nav.Link as={Link} to="/users">
                      Користувачі
                    </Nav.Link>
                  </>
                ) : (
                  <></>
                )}
              </Nav>
            </Navbar.Collapse>
            {role === "" ? (
              <Login setRole={setRole} />
            ) : (
              <Button className="me-2" variant="outline-danger" onClick={() => client().logout()}>
                Вийти
              </Button>
            )}
          </Navbar>
          <div className="container mt-4">
            <Routes>
              <Route path="/" element={<Outlet />}>
                <Route index element={<Monuments />} />
                {role === "User" || role === "Admin" ? (
                  <>
                    <Route path="/orders" element={<Orders />} />
                    <Route
                      path="/admincommissionprotocols"
                      element={<AdminCommissionProtocols />}
                    />
                  </>
                ) : (
                  <></>
                )}
                {role === "Admin" ? (
                  <>
                    <Route path="/users" element={<Users />} />
                  </>
                ) : (
                  <></>
                )}
                {/* <Route path="/login" element={<Login />} /> */}
                <Route path="*" element={<NotFound />} />
              </Route>
            </Routes>
          </div>
          <ToastContainer position="bottom-start" className="p-3">
            {toasts.map(toast => (
              <Toast
                key={toast.id}
                bg={toast.variant}
                onClose={() => setToasts(toasts.filter(t => t.id !== toast.id))}
                delay={5000}
                autohide
              >
                <Toast.Header>
                  <strong className="me-auto">{toast.title}</strong>
                </Toast.Header>
                {/* <Toast.Body>{toast.message}</Toast.Body> */}
              </Toast>
            ))}
          </ToastContainer>
        </Router>
      </ToastContext.Provider>
    </UserContext.Provider>
  );
}
export default App;
