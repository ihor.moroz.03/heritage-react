import React, { useEffect, useState } from "react";
import { Form, Row } from "react-bootstrap";

function ImportSelector({ sheet, onChange, label = sheet.name }) {
  const [cols, setCols] = useState([]);
  const [delimeter, setDelimeter] = useState(", ");

  const getKey = i => cols.map(col => sheet.data[i][col]).join(delimeter);

  useEffect(() => {
    onChange(cols.length > 0, sheet, getKey);
  }, [cols, delimeter]);

  return (
    <>
      <Form.Group>
        <Form.Label>
          {label}
          {cols.length > 0 ? ` (Приклад: ${getKey(0)})` : null}
        </Form.Label>
        {cols.map((col, i) => (
          <Form.Select
            key={`col${i}`}
            value={col}
            onChange={({ target }) => {
              if (target.value.length === 0) {
                setCols(cols.filter(c => c !== col));
                return;
              }
              let newCols = [...cols];
              newCols[i] = target.value;
              setCols(newCols);
            }}
          >
            <option value=""></option>
            {Object.keys(sheet.data[0]).map((field, j) => (
              <option key={`col${i}field${j}`}>{field}</option>
            ))}
          </Form.Select>
        ))}
        <Form.Select
          onChange={({ target }) => {
            if (target.value.length > 0) {
              setCols([...cols, target.value]);
              target.value = "";
            }
          }}
        >
          <option value=""></option>
          {Object.keys(sheet.data[0]).map((col, i) => (
            <option key={`opt${i}`}>{col}</option>
          ))}
        </Form.Select>
      </Form.Group>
      {cols.length > 1 ? (
        <Form.Group controlId="delimeter">
          <Form.Label>Деліметер</Form.Label>
          <Form.Control
            type="text"
            name="delimeter"
            value={delimeter}
            onChange={({ target }) => setDelimeter(target.value)}
          />
        </Form.Group>
      ) : null}
    </>
  );
}

export default ImportSelector;
