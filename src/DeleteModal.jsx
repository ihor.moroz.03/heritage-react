import React, { useContext, useState } from "react";
import { Form, Button, Modal } from "react-bootstrap";
import { client } from "./client";
import { MdDelete } from "react-icons/md";
import { ToastContext } from "./App";

const DeleteModal = ({ deleteId, apiPath }) => {
  const [show, setShow] = useState(false);
  const addToast = useContext(ToastContext);

  const handleSubmit = async e => {
    e.preventDefault();
    try {
      console.log(await client().del(`api/${apiPath}/${deleteId}`));

      addToast(`Запис успішно видалено`, "Form submitted successfully", "success");
      handleClose();
    } catch (err) {
      console.log(err);
      addToast("Помилка видалення", "Error submitting form", "danger");
    }
  };

  const handleClose = () => {
    setShow(false);
  };

  return (
    <>
      <Button className="text-nowrap" onClick={() => setShow(true)}>
        <MdDelete color="red" />
        Видалити
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Попередження</Modal.Title>
        </Modal.Header>
        <Form onSubmit={handleSubmit}>
          <Modal.Body>Після видалення повернути дані неможливо</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Скасувати
            </Button>
            <Button variant="danger" type="submit">
              Видалити
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default DeleteModal;
