import React, { useState } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { client } from "./client";
import { useNavigate } from "react-router";
import Modal from "react-bootstrap/Modal";

const Login = ({ setRole }) => {
  const [show, setShow] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = event => {
    event.preventDefault();
    client()
      .login(email, password)
      .then(data => setRole(data.role))
      .catch(err => {
        console.log(err);
      });
    setShow(false);
  };

  return (
    <>
      <Button className="me-2" variant="outline-success" onClick={() => setShow(true)}>
        Авторизуватися
      </Button>

      <Modal show={show} onHide={() => setShow(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Авторизація</Modal.Title>
        </Modal.Header>
        <Form onSubmit={handleSubmit}>
          <Modal.Body>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Електронна адреса</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={e => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Пароль</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={e => setPassword(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit">
              Авторизуватися
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default Login;
