import React, { useContext, useEffect, useState } from "react";
import { Form, Button, Row, Col, Modal } from "react-bootstrap";
import { client } from "./client";
import { MdEdit } from "react-icons/md";
import { ToastContext } from "./App";
import { balconyTranslate } from "./helpers";

const MonumentFormModal = ({
  data = {
    name: "",
    address: {
      addressLine1: "",
      city: "",
      zip: "",
    },
    dates: "",
    type: "",
    protectionDecision: "",
    protectionNumber: "",
    nationalRegisterDocs: "",
    category: "",
    generalCondition: "",
    balconyCondition: "None",
  },
  updateId,
}) => {
  const [show, setShow] = useState(false);
  const [formData, setFormData] = useState(data);
  const addToast = useContext(ToastContext);

  const handleChange = e => {
    const { name, value } = e.target;
    if (name.includes(".")) {
      const [main, sub] = name.split(".");
      setFormData(prevState => ({
        ...prevState,
        [main]: {
          ...prevState[main],
          [sub]: value,
        },
      }));
    } else {
      setFormData(prevState => ({
        ...prevState,
        [name]: value,
      }));
    }
    console.log(formData);
  };

  const handleSubmit = async e => {
    e.preventDefault();
    try {
      let res;
      if (updateId) {
        const putFormData = { ...formData, id: updateId };
        putFormData.address.id = updateId;
        res = await client().put(`api/monuments/${updateId}`, putFormData);
      } else {
        res = await client().post("api/monuments", formData);
      }
      console.log(res);
      addToast(
        `Успішно ${updateId ? "оновлено" : "додано"}`,
        "Form submitted successfully",
        "success"
      );
      handleClose();
    } catch (err) {
      console.log(err);
      addToast("Помилка", "Error submitting form", "danger");
    }
    console.log(JSON.stringify(formData));
  };

  const handleClose = () => {
    setShow(false);
    setFormData(data); // Reset form data when modal is closed
  };

  return (
    <>
      {updateId ? (
        <Button className="text-nowrap" onClick={() => setShow(true)}>
          <MdEdit />
          Редагувати
        </Button>
      ) : (
        <Button className="me-2" onClick={() => setShow(true)}>
          Додати
        </Button>
      )}

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Нова пам'ятка</Modal.Title>
        </Modal.Header>
        <Form onSubmit={handleSubmit}>
          <Modal.Body>
            <Row>
              <Col md={6}>
                <Form.Group controlId="name">
                  <Form.Label>Назва</Form.Label>
                  <Form.Control
                    type="text"
                    name="name"
                    value={formData.name}
                    onChange={handleChange}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <Form.Group controlId="addressLine1">
                  <Form.Label>Адреса</Form.Label>
                  <Form.Control
                    type="text"
                    name="address.addressLine1"
                    value={formData.address.addressLine1}
                    onChange={handleChange}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group controlId="city">
                  <Form.Label>Місто</Form.Label>
                  <Form.Control
                    type="text"
                    name="address.city"
                    value={formData.address.city}
                    onChange={handleChange}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group controlId="zip">
                  <Form.Label>Поштовий індекс</Form.Label>
                  <Form.Control
                    type="text"
                    name="address.zip"
                    value={formData.address.zip}
                    onChange={handleChange}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <Form.Group controlId="dates">
                  <Form.Label>Дата спорудження</Form.Label>
                  <Form.Control
                    type="text"
                    name="dates"
                    value={formData.dates}
                    onChange={handleChange}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group controlId="type">
                  <Form.Label>Вид пам'ятки</Form.Label>
                  <Form.Control
                    type="text"
                    name="type"
                    value={formData.type}
                    onChange={handleChange}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <Form.Group controlId="protectionDecision">
                  <Form.Label>Розпорядження про захист</Form.Label>
                  <Form.Control
                    type="text"
                    name="protectionDecision"
                    value={formData.protectionDecision}
                    onChange={handleChange}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group controlId="protectionNumber">
                  <Form.Label>Номер</Form.Label>
                  <Form.Control
                    type="text"
                    name="protectionNumber"
                    value={formData.protectionNumber}
                    onChange={handleChange}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <Form.Group controlId="nationalRegisterDocs">
                  <Form.Label>Наказ про внесення до реєстру</Form.Label>
                  <Form.Control
                    type="text"
                    name="nationalRegisterDocs"
                    value={formData.nationalRegisterDocs}
                    onChange={handleChange}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group controlId="category">
                  <Form.Label>Категорія</Form.Label>
                  <Form.Control
                    type="text"
                    name="category"
                    value={formData.category}
                    onChange={handleChange}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <Form.Group controlId="generalCondition">
                  <Form.Label>Загальний стан</Form.Label>
                  <Form.Control
                    type="text"
                    name="generalCondition"
                    value={formData.generalCondition}
                    onChange={handleChange}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group controlId="balconyCondition">
                  <Form.Label>Стан балконів</Form.Label>
                  <Form.Control
                    as="select"
                    name="balconyCondition"
                    value={formData.balconyCondition}
                    onChange={handleChange}
                  >
                    <option value="None">Відсутні</option>
                    <option value="Good">Добрий</option>
                    <option value="Satisfactory">Задовільний</option>
                    <option value="UnSatisfactory">Незадовільний</option>
                    <option value="Wreck">Аварійний</option>
                  </Form.Control>
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Скасувати
            </Button>
            <Button variant="primary" type="submit">
              Додати
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default MonumentFormModal;
