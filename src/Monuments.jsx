import React, { useContext, useEffect, useState } from "react";
import { Button, Container, Row } from "react-bootstrap";
import MonumentFormModal from "./MonumentFormModal";
import { client } from "./client";
import MonumentList from "./MonumentList";
import MonumentImportModal from "./MonumentImportModal";
import { UserContext } from "./App";

function Monuments() {
  const [monuments, setMonuments] = useState([]);
  const { role } = useContext(UserContext);

  //move this to MonumentList and add toasts
  useEffect(() => {
    client()
      .get("api/monuments")
      .then(res => setMonuments(res))
      .catch(err => console.log(err));
  }, []);

  return (
    <Container>
      <div className="mb-3">
        {role !== "" ? (
          <>
            <MonumentFormModal />
            <MonumentImportModal />
          </>
        ) : null}
      </div>
      <Row>
        <MonumentList data={monuments} />
      </Row>
    </Container>
  );
}

export default Monuments;
