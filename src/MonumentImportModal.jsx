import React, { useContext, useEffect, useState } from "react";
import { Form, Button, Modal, Row, Col } from "react-bootstrap";
import { client } from "./client";
import { MdFileOpen } from "react-icons/md";
import { ToastContext } from "./App";
import { read, utils, writeFile } from "xlsx";
import ImportSelector from "./ImportSelector";
import { balconyTranslate } from "./helpers";
import MonumentList from "./MonumentList";

const MonumentImportModal = () => {
  const [show, setShow] = useState(false);
  const [sheets, setSheets] = useState([]);
  const [selectedSheets, setSelectedSheets] = useState(new Set());
  const addToast = useContext(ToastContext);
  const [combinedSheet, setCombinedSheet] = useState({ name: "", data: [] });
  const [step, setStep] = useState(0);
  const defaultKeyMap = {
    name: () => "",
    address: {
      addressLine1: () => "",
      city: () => "",
      zip: () => "",
    },
    dates: () => "",
    type: () => "",
    protectionDecision: () => "",
    protectionNumber: () => "",
    nationalRegisterDocs: () => "",
    category: () => "",
    generalCondition: () => "",
    balconyCondition: () => "Відсутні",
  };
  const [keyMap, setKeyMap] = useState({ ...defaultKeyMap });
  const [finalRows, setFinalRows] = useState();

  const getFinalRow = i => ({
    name: keyMap.name(i),
    address: {
      addressLine1: keyMap.address.addressLine1(i),
      city: keyMap.address.city(i),
      zip: keyMap.address.zip(i),
    },
    dates: keyMap.dates(i),
    type: keyMap.type(i),
    protectionDecision: keyMap.protectionDecision(i),
    protectionNumber: keyMap.protectionNumber(i),
    nationalRegisterDocs: keyMap.nationalRegisterDocs(i),
    category: keyMap.category(i),
    generalCondition: keyMap.generalCondition(i),
    balconyCondition: balconyTranslate(keyMap.balconyCondition(i)),
  });

  const handleSubmit = async () => {
    setFinalRows(combinedSheet.data.map((_, i) => getFinalRow(i)));

    try {
      console.log(finalRows.forEach(r => client().post("/api/monuments", r)));
      addToast(
        `Успішно імпортовано ${finalRows.length} записів`,
        "Form submitted successfully",
        "success"
      );
      handleClose();
    } catch (err) {
      console.log(err);
      addToast("Помилка імпорту", "Error submitting form", "danger");
    }
  };

  const updateFiles = async files => {
    if (!files || files.length === 0) return;
    const newSheets = [];

    for (const file of files) {
      if (file.name.endsWith(".xlsx") || file.name.endsWith(".xls")) {
        const ab = await file.arrayBuffer();
        const wb = read(ab);
        newSheets.push(
          ...wb.SheetNames.map(sn => ({
            name: sn,
            data: utils.sheet_to_json(wb.Sheets[sn]),
          })).filter(s => s.data.length > 0)
        );
      }
    }

    setSheets([...sheets, ...newSheets]);
  };

  const handleClose = () => {
    setFinalRows(null);
    setShow(false);
    setStep(0);
    setSelectedSheets(new Set());
    setSheets([]);
  };

  const handleImportSelectorChange = (selected, sheet, getKey) => {
    let newSet = new Set([...selectedSheets]);
    if (!selected) {
      newSet.delete(sheet);
      setSelectedSheets(newSet);
      return;
    }
    sheet.getKey = getKey;
    if (!selectedSheets.has(sheet)) {
      newSet.add(sheet);
      setSelectedSheets(newSet);
    }
  };

  const combineSelectedSheets = () => {
    const selectedSheetsArray = [...selectedSheets];
    let leftover = selectedSheetsArray.reduce((obj, sheet) => ((obj[sheet.name] = []), obj), {});
    let combinedSheetDictionary = {};

    for (let { data, getKey } of selectedSheets) {
      for (let i in data) {
        const key = getKey(i);

        if (combinedSheetDictionary.hasOwnProperty(key)) continue;
        let rows = selectedSheetsArray.map(s => ({
          sname: s.name,
          row: s.data.find((_, j) => key === s.getKey(j)),
        }));
        rows = rows.filter(r => r.row !== undefined);
        if (rows.length === selectedSheetsArray.length)
          combinedSheetDictionary[key] = rows
            .map(r => r.row)
            .reduce((combined, current) => ({ ...combined, ...current }), {});
        else rows.forEach(r => leftover[r.sname].push(r.row));
      }
    }

    setCombinedSheet({
      name: "combined",
      data: Object.values(combinedSheetDictionary),
    });
    setStep(1);

    const wb = utils.book_new();
    for (let name in leftover) {
      const ws = utils.json_to_sheet(leftover[name]);
      utils.book_append_sheet(wb, ws, name);
    }
    writeFile(wb, "leftover.xlsx");
  };

  return (
    <>
      <Button className="text-nowrap" onClick={() => setShow(true)}>
        <MdFileOpen />
        Імпорт записів з файлів
      </Button>

      {step === 0 ? (
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Виберіть файли і аркуші</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form className="d-flex mb-3">
              <Form.Control
                type="file"
                className="me-2"
                aria-label="Files"
                onChange={e => updateFiles(e.target.files)}
                multiple
              />
            </Form>
            <Form>
              {sheets.map((sheet, i) => (
                <ImportSelector
                  key={`wb${i}`}
                  sheet={sheet}
                  onChange={handleImportSelectorChange}
                />
              ))}
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Скасувати
            </Button>
            <Button
              variant="primary"
              disabled={selectedSheets.size < 1}
              onClick={combineSelectedSheets}
            >
              Далі
            </Button>
          </Modal.Footer>
        </Modal>
      ) : (
        //step 1:
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Виберіть відповідні колонки для полів</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Row>
                <Col md={6}>
                  <ImportSelector
                    sheet={combinedSheet}
                    label="Назва"
                    onChange={(selected, _, getKey) =>
                      setKeyMap({ ...keyMap, name: selected ? getKey : defaultKeyMap.name })
                    }
                  />
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <ImportSelector
                    sheet={combinedSheet}
                    label="Адреса"
                    onChange={(selected, _, getKey) =>
                      setKeyMap({
                        ...keyMap,
                        address: {
                          ...keyMap.address,
                          addressLine1: selected ? getKey : defaultKeyMap.address.addressLine1,
                        },
                      })
                    }
                  />
                </Col>
                <Col md={6}>
                  <ImportSelector
                    sheet={combinedSheet}
                    label="Місто"
                    onChange={(selected, _, getKey) =>
                      setKeyMap({
                        ...keyMap,
                        address: {
                          ...keyMap.address,
                          city: selected ? getKey : defaultKeyMap.address.city,
                        },
                      })
                    }
                  />
                </Col>
                <Col md={6}>
                  <ImportSelector
                    sheet={combinedSheet}
                    label="Поштовий індекс"
                    onChange={(selected, _, getKey) =>
                      setKeyMap({
                        ...keyMap,
                        address: {
                          ...keyMap.address,
                          zip: selected ? getKey : defaultKeyMap.address.zip,
                        },
                      })
                    }
                  />
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <ImportSelector
                    sheet={combinedSheet}
                    label="Дата спорудження"
                    onChange={(selected, _, getKey) =>
                      setKeyMap({ ...keyMap, dates: selected ? getKey : defaultKeyMap.dates })
                    }
                  />
                </Col>
                <Col md={6}>
                  <ImportSelector
                    sheet={combinedSheet}
                    label="Вид пам'ятки"
                    onChange={(selected, _, getKey) =>
                      setKeyMap({ ...keyMap, type: selected ? getKey : defaultKeyMap.type })
                    }
                  />
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <ImportSelector
                    sheet={combinedSheet}
                    label="Розпорядження про захист"
                    onChange={(selected, _, getKey) =>
                      setKeyMap({
                        ...keyMap,
                        protectionDecision: selected ? getKey : defaultKeyMap.protectionDecision,
                      })
                    }
                  />
                </Col>
                <Col md={6}>
                  <ImportSelector
                    sheet={combinedSheet}
                    label="Номер"
                    onChange={(selected, _, getKey) =>
                      setKeyMap({
                        ...keyMap,
                        protectionNumber: selected ? getKey : defaultKeyMap.protectionNumber,
                      })
                    }
                  />
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <ImportSelector
                    sheet={combinedSheet}
                    label="Наказ про внесення до реєстру"
                    onChange={(selected, _, getKey) =>
                      setKeyMap({
                        ...keyMap,
                        nationalRegisterDocs: selected
                          ? getKey
                          : defaultKeyMap.nationalRegisterDocs,
                      })
                    }
                  />
                </Col>
                <Col md={6}>
                  <ImportSelector
                    sheet={combinedSheet}
                    label="Категорія"
                    onChange={(selected, _, getKey) =>
                      setKeyMap({ ...keyMap, category: selected ? getKey : defaultKeyMap.category })
                    }
                  />
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <ImportSelector
                    sheet={combinedSheet}
                    label="Загальний стан"
                    onChange={(selected, _, getKey) =>
                      setKeyMap({
                        ...keyMap,
                        generalCondition: selected ? getKey : defaultKeyMap.generalCondition,
                      })
                    }
                  />
                </Col>
                <Col md={6}>
                  <ImportSelector
                    sheet={combinedSheet}
                    label="Стан балконів"
                    onChange={(selected, _, getKey) =>
                      setKeyMap({
                        ...keyMap,
                        balconyCondition: selected ? getKey : defaultKeyMap.balconyCondition,
                      })
                    }
                  />
                </Col>
              </Row>
            </Form>
            {finalRows ? (
              <MonumentList data={finalRows} showSearch={false} showActions={false} />
            ) : null}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Скасувати
            </Button>
            <Button
              variant="secondary"
              onClick={() => setFinalRows(combinedSheet.data.map((_, i) => getFinalRow(i)))}
            >
              Попередній перегляд
            </Button>
            <Button variant="primary" onClick={handleSubmit}>
              Імпортувати
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </>
  );
};

export default MonumentImportModal;
