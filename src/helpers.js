export const balconyConditionUkMap = {
  None: "Відсутні",
  Good: "Добрий",
  Satisfactory: "Задовільний",
  UnSatisfactory: "Незадовільний",
  Wreck: "Аварійний",
};

export const balconyConditionDict = {
  "": "",
  ...balconyConditionUkMap,
  ...Object.fromEntries(Object.entries(balconyConditionUkMap).map(a => a.reverse())),
};

const balconyTranslateFormat = s =>
  s.length > 0 ? `${s[0].toUpperCase()}${s.substr(1).toLowerCase()}` : "";

export const balconyTranslate = s => balconyConditionDict[balconyTranslateFormat(s)];
