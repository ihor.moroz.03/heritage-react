import React, { useContext, useState } from "react";
import Table from "react-bootstrap/Table";
import { balconyConditionUkMap } from "./helpers";
import { Button, Form } from "react-bootstrap";
import { MdFilePresent } from "react-icons/md";
import MonumentFormModal from "./MonumentFormModal";
import DeleteModal from "./DeleteModal";
import { UserContext } from "./App";
import MyMap from "./MyMap";

const objectValuesFlat = o => {
  let res = Object.values(o);

  while (res.some(o => typeof o === "object"))
    res = res.flatMap(val => (typeof val !== "object" ? val : Object.values(val)));

  return res;
};

const MonumentList = ({ data, showSearch = true, showActions = true, showMap = false }) => {
  const [searchText, setSearchText] = useState("");
  const { role } = useContext(UserContext);

  return (
    <>
      {showSearch ? (
        <Form className="d-flex mb-3">
          <Form.Control
            type="search"
            placeholder="Пошук"
            className="me-2"
            aria-label="Search"
            onChange={e => setSearchText(e.target.value)}
          />
        </Form>
      ) : null}
      {showMap ? <MyMap data={data} /> : null}
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            {role !== "" && showActions ? <th>Дії</th> : null}
            <th>Назва</th>
            <th>Адреса</th>
            <th>Місто</th>
            <th>Поштовий індекс</th>
            <th>Дата спорудження</th>
            <th>Вид пам'ятки</th>
            <th>Розпорядження про захист</th>
            <th>Номер</th>
            <th>Наказ про внесення до реєстру</th>
            <th>Категорія</th>
            <th>Загальний стан</th>
            <th>Стан балконів</th>
          </tr>
        </thead>
        <tbody>
          {data
            // .map(item => {
            //   return { ...item, balconyCondition: balconyConditionUkMap[item.balconyCondition] };
            // })
            .filter(item => {
              const it = objectValuesFlat(item);
              it.balconyCondition = balconyConditionUkMap[it.balconyCondition];
              return searchText.length < 3 || it.some(field => String(field).includes(searchText));
            })
            .map((item, index) => (
              <tr key={index}>
                {role !== "" && showActions ? (
                  <td>
                    <Button className="text-nowrap">
                      <MdFilePresent />
                      Файли
                    </Button>{" "}
                    <MonumentFormModal updateId={item.id} data={item} />{" "}
                    <DeleteModal apiPath="monuments" deleteId={item.id} />
                  </td>
                ) : null}
                <td>{item.name}</td>
                <td>{item.address.addressLine1}</td>
                <td>{item.address.city}</td>
                <td>{item.address.zip}</td>
                <td>{item.dates}</td>
                <td>{item.type}</td>
                <td>{item.protectionDecision}</td>
                <td>{item.protectionNumber}</td>
                <td>{item.nationalRegisterDocs}</td>
                <td>{item.category}</td>
                <td>{item.generalCondition}</td>
                <td>{balconyConditionUkMap[item.balconyCondition]}</td>
              </tr>
            ))}
        </tbody>
      </Table>
    </>
  );
};

export default MonumentList;
