import React, { useEffect, useState } from "react";
import { MapContainer, Marker, Popup, TileLayer, useMap } from "react-leaflet";

function MyMap({ data }) {
  const [markers, setMarkers] = useState([]);

  useEffect(() => {
    // let posArr = [];
    // for (let { address, name } of data) {
    // fetch(
    //   `https://nominatim.openstreetmap.org/search?street=${address.addressLine1}&city=${address.city}&format=jsonv2&limit=1`
    // )
    //   .then(response => response.json())
    //   .then(res => posArr.push({ pos: [res[0].lat, res[0].lon] }))
    //   .catch(err => console.log(err));
    // // }
    // console.log(posArr);
  }, [data]);

  return (
    <MapContainer center={[49.842957, 24.031111]} zoom={13} scrollWheelZoom={false}>
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={[49.842957, 24.031111]}>
        <Popup>Lviv</Popup>
      </Marker>
      {markers.map((marker, i) => (
        <Marker key={`mark${i}`} position={marker.pos}></Marker>
      ))}
    </MapContainer>
  );
}

export default MyMap;
